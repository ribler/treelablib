﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeLabLib
{
    public class BinaryTree<KeyType, ValueType>
           where KeyType : IComparable<KeyType>
    {
        class Node
        {
            Node[] child = new Node[2] { null, null };
            enum Child { Left = 0, Right = 1 };
            public KeyType Key;
            public ValueType Value;

            public Node(KeyType key, ValueType value)
            {
                Key = key;
                Value = value;
            }

            public Node LeftChild
            {
                get
                {
                    return child[(int)Child.Left];
                }
                set
                {
                    child[(int)Child.Left] = value;
                }
            }

            public Node RightChild
            {
                get
                {
                    return child[(int)Child.Right];
                }
                set
                {
                    child[(int)Child.Right] = value;
                }
            }
        }
    }
}
