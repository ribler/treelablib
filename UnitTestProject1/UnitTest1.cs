﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TreeLabLib;

namespace TestTrees
{
    [TestClass]
    public class TestBinaryTree
    {
        //[TestMethod]
        //public void HeightOfEmptyTree()
        //{
        //    BinaryTree<string, int> emptyTree = new BinaryTree<string, int>();
        //    Assert.AreEqual(-1, emptyTree.Height());
        //}

        //[TestMethod]
        //public void InsertOneNodeTest()
        //{
        //    BinaryTree<string, int> tree = new BinaryTree<string, int>();
        //    tree.Insert("abc", 10);
        //    Assert.AreEqual(0, tree.Height());
        //}

        //[TestMethod]
        //public void InsertTestThreeNodesTest()
        //{
        //    BinaryTree<string, int> tree = new BinaryTree<string, int>();
        //    tree.Insert("abc", 10);
        //    tree.Insert("bcd", 11);
        //    tree.Insert("cde", 12);
        //    Assert.AreEqual(2, tree.Height());
        //}

        //[TestMethod]
        //public void smallInOrderTraversal()
        //{
        //    BinaryTree<string, int> tree = new BinaryTree<string, int>();
        //    tree.Insert("j", 10);
        //    tree.Insert("d", 11);
        //    tree.Insert("r", 12);

        //    string[] key = new string[] { "d", "j", "r" };
        //    int[] value = new int[] { 11, 10, 12 };

        //    int result = 0;

        //    foreach (Tuple<string, int> nodeValue in tree.InOrderTraversal())
        //    {
        //        Assert.AreEqual(key[result].ToString(), nodeValue.Item1);
        //        Assert.AreEqual(value[result++], nodeValue.Item2);
        //    }
        //}


        //[TestMethod]
        //public void largeInOrderTraversal()
        //{
        //    const int PAYLOAD = 10;
        //    BinaryTree<string, int> tree = new BinaryTree<string, int>();
        //    tree.Insert("u", PAYLOAD);
        //    tree.Insert("n", PAYLOAD);
        //    tree.Insert("i", PAYLOAD);
        //    tree.Insert("v", PAYLOAD);
        //    tree.Insert("e", PAYLOAD);
        //    tree.Insert("r", PAYLOAD);
        //    tree.Insert("s", PAYLOAD);
        //    tree.Insert("t", PAYLOAD);
        //    tree.Insert("y", PAYLOAD);
        //    tree.Insert("o", PAYLOAD);
        //    tree.Insert("f", PAYLOAD);
        //    tree.Insert("l", PAYLOAD);
        //    tree.Insert("c", PAYLOAD);
        //    tree.Insert("h", PAYLOAD);
        //    tree.Insert("b", PAYLOAD);
        //    tree.Insert("g", PAYLOAD);

        //    // Inorder traversal of binary tree should yield a sorted order
        //    string name = "universtyoflchbg";
        //    char[] charArray = name.ToCharArray();
        //    Array.Sort(charArray);

        //    int result = 0;
        //    foreach (Tuple<string, int> nodeTuple in tree.InorderTraversal())
        //    {
        //        Assert.AreEqual(charArray[result++].ToString(), nodeTuple.Item1);
        //        Assert.AreEqual(PAYLOAD, nodeTuple.Item2);
        //    }
        //}

        //[TestMethod]
        //public void noPreorderTraversal()
        //{
        //    BinaryTree<string, int> tree = new BinaryTree<string, int>();
        //    foreach (Tuple<string, int> nodeTuple in tree.PreOrderTraversal())
        //    {
        //        Assert.Fail();
        //    }
        //}

        //[TestMethod]
        //public void insertAtRootTest()
        //{
        //    BinaryTree<string, int> tree = new BinaryTree<string, int>();
        //    tree.InsertAtRoot("J", 10);
        //    Assert.AreEqual("J", tree.Root.Key);
        //    tree.InsertAtRoot("E", 10);
        //    Assert.AreEqual("E", tree.Root.Key);
        //    tree.InsertAtRoot("P", 10);
        //    Assert.AreEqual("P", tree.Root.Key);
        //    tree.InsertAtRoot("A", 10);
        //    Assert.AreEqual("A", tree.Root.Key);
        //    tree.InsertAtRoot("C", 10);
        //    Assert.AreEqual("C", tree.Root.Key);
        //    tree.InsertAtRoot("M", 10);
        //    Assert.AreEqual("M", tree.Root.Key);
        //    tree.InsertAtRoot("S", 10);
        //    Assert.AreEqual("S", tree.Root.Key);
        //}
    }
}
